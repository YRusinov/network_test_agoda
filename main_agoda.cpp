/*
 * =====================================================================================
 *
 *       Filename:  main_adoga.cpp
 *
 *    Description:  Home test for Agoda
 *
 *        Version:  1.0
 *        Created:  24.06.2018 17:58:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Yuriy Rusinov (), yrusinov@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <curl/curl.h>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::stringstream;

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

int main (int argc, char* argv[])
{
    if (argc < 2)
    {
        cerr << "File with urls needed for input. Exiting" << endl;
        return 1;
    }
    std::ifstream url_file (argv[1]);
    vector<string> url_list;
    while (!url_file.eof())
    {
        string fstr;
        getline (url_file, fstr);
        cout << fstr << endl;
        url_list.push_back (fstr);
    }
    CURL *curl = curl_easy_init();
    if (!curl)
    {
        cerr << "Cannot init libcurl. Exiting" << endl;
        return 2;
    }
    int n = url_list.size();
    for (int i=0; i<n; i++)
    {
        string fName;
        size_t sInd = url_list[i].find_last_of ('/');
        fName = url_list[i].substr (sInd+1);
        FILE * fp = fopen (fName.c_str(), "wb");
        cout << url_list[i].c_str() << ' ' << fName << endl;
        CURLcode res = curl_easy_setopt (curl, CURLOPT_URL, url_list[i].c_str());
        if (res != CURLE_OK)
        {
            cerr << "Cannot set URL, error code = " << res << endl;
            fclose (fp);
            continue;
        }
        res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        if (res != CURLE_OK)
        {
            cerr << "Cannot set write function, error code = " << res << endl;
            continue;
        }
        res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        if (res != CURLE_OK)
        {
            cerr << "Cannot set write data, error code = " << res << endl;
            continue;
        }
        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
        {
            cerr << "Cannot write data, error code = " << res << endl;
            continue;
        }
        res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, 0);
        fclose (fp);
    }

    curl_easy_cleanup(curl);
}
